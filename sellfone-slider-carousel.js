import { html, LitElement } from 'lit-element';
import style from './sellfone-slider-carousel-styles.js';
import '@catsys/product-component/product-component.js';

class SellfoneSliderCarousel extends LitElement {
  static get properties() {
    return {
      productList:{
        type:Array,
        attribute:"product-list"
      },
      scrollQty:{
        type:Number
      }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.productList=[];
    this.scrollQty=0;
  }

  render() {
    return html`
      <div class="mainContainer">
        <div class="sliderContainer animated fadeInLeft">
          ${this.productList.map(product=>{
            return html` <product-component class="product" .objectCell="${product}" @product-selected="${this.productSelected}"></product-component>`
          })}
          
        </div>
        <a class="prev" @click="${(e)=>this.scrollContainer(-220)}">&#10094;</a>
          <a class="next" @click="${(e)=>this.scrollContainer(220)}">&#10095;</a>
      </div>
       
      `;
    }
    
    scrollContainer(amount){
      const slider=this.shadowRoot.querySelector(".sliderContainer");
      let scrollLeftMax=slider.scrollWidth - slider.clientWidth;
      if(this.scrollQty<scrollLeftMax){
        this.scrollQty+=amount;
        slider.scroll({left:this.scrollQty,behavior:'smooth'});  
      }else{
        this.scrollQty=0;
        slider.scroll({left:0,behavior:'smooth'});  
      }  
    }

    productSelected(event){
      this.dispatchEvent(new CustomEvent("product-selected",{
        bubbles:false,
        composed:false,
        detail:event.detail
      }));
    }
    
}

window.customElements.define("sellfone-slider-carousel", SellfoneSliderCarousel);
