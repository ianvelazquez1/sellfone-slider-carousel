/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-slider-carousel.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-slider-carousel></sellfone-slider-carousel>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
